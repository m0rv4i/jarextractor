/**
 *  Copyright Murex S.A.S., 2003-2014. All Rights Reserved.
 * 
 *  This software program is proprietary and confidential to Murex S.A.S and its affiliates ("Murex") and, without limiting the generality of the foregoing reservation of rights, shall not be accessed, used, reproduced or distributed without the
 *  express prior written consent of Murex and subject to the applicable Murex licensing terms. Any modification or removal of this copyright notice is expressly prohibited.
 */
package com.rbone.jarextractor;

import java.text.MessageFormat;

import java.util.HashMap;
import java.util.Map;


/**
 * Parses command line arguments.
 *
 * @author Murex integration team
 */
final class ArgumentParser {

    //~ ----------------------------------------------------------------------------------------------------------------
    //~ Constructors 
    //~ ----------------------------------------------------------------------------------------------------------------

    private ArgumentParser() {
        throw new UnsupportedOperationException("This class is a utility class and should not be instantiated");
    }

    //~ ----------------------------------------------------------------------------------------------------------------
    //~ Methods 
    //~ ----------------------------------------------------------------------------------------------------------------

    /**
     * Parses command line arguments in the form -argName=argvalue and puts them in a {@link Map}.
     *
     * @param  args -the arguments to parse
     *
     * @return - the parsed arguments
     */
    public static Map<String, String> parseArguments(final String[] args) {
        final Map<String, String> parsedArguments = new HashMap<String, String>();
        for (final String arg : args) {
            if (!arg.startsWith("-") || !arg.contains("=")) {
                System.err.println(MessageFormat.format("Invalid argument: {0}, expected format is -argName=argvalue", arg));
            } else {
                final String[] parsedArg = arg.split("=");
                final String key = parsedArg[0].substring(1);
                final String value = parsedArg[1];
                System.out.println(MessageFormat.format("Found argument with key: {0}, and value: {1}", key, value));
                parsedArguments.put(key, value);
            }
        }
        return parsedArguments;
    }

}
