/**
 *  Copyright Murex S.A.S., 2003-2014. All Rights Reserved.
 * 
 *  This software program is proprietary and confidential to Murex S.A.S and its affiliates ("Murex") and, without limiting the generality of the foregoing reservation of rights, shall not be accessed, used, reproduced or distributed without the
 *  express prior written consent of Murex and subject to the applicable Murex licensing terms. Any modification or removal of this copyright notice is expressly prohibited.
 */
package com.rbone.jarextractor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Recursively extracts all none-test jars in the start directory to a location.
 *
 * @author Murex integration team
 */
final class JarExtractor {

    //~ ----------------------------------------------------------------------------------------------------------------
    //~ Static fields/initializers 
    //~ ----------------------------------------------------------------------------------------------------------------

    private static final boolean OVERWRITE_EXISTING = true;

    //~ ----------------------------------------------------------------------------------------------------------------
    //~ Instance fields 
    //~ ----------------------------------------------------------------------------------------------------------------

    private final File outputDir;
    private final File startDir;

    //~ ----------------------------------------------------------------------------------------------------------------
    //~ Constructors 
    //~ ----------------------------------------------------------------------------------------------------------------

    /**
     * @param startDirPath  - the path to the start directory
     * @param outputDirPath - the path to the file to log the output
     */
    public JarExtractor(final String startDirPath, final String outputDirPath) {
        this.startDir = new File(startDirPath);
        this.outputDir = new File(outputDirPath);
    }

    //~ ----------------------------------------------------------------------------------------------------------------
    //~ Methods 
    //~ ----------------------------------------------------------------------------------------------------------------

    /**
     * Start the process.
     */
    public void start() {
        System.out.println("Working...");
        if (!this.outputDir.exists() && !this.outputDir.mkdirs()) {
            throw new IllegalStateException("Could not create output directory: " + this.outputDir);
        }
        this.extractJars(this.startDir);
        System.out.println("Finished.");
    }

    /**
     * @param  directory - the directory having its items logged
     *
     * @throws IOException - when writing to the file errors
     */
    private void extractJars(final File directory) {
        for (final File file : directory.listFiles()) {
            if (file.isDirectory()) {
                this.extractJars(file);
            } else {
                if (file.getName().endsWith(".jar") && !file.getName().endsWith("-tests.jar")) {
                    try {
                        InputStream inputStream = new FileInputStream(file);
                        OutputStream outputStream = new FileOutputStream(this.outputDir + File.separator + file.getName(), OVERWRITE_EXISTING);
                        byte[] buffer = new byte[1024];
                        int length;
                        try {
                            while ((length = inputStream.read(buffer)) > 0) {
                                outputStream.write(buffer, 0, length);
                            }
                        } finally {
                            inputStream.close();
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        System.err.println("Error while copying file: " + file + ", " + e.getMessage());
                    }
                    System.out.println("File copied: " + file);
                }
            }
        }
    }

}
