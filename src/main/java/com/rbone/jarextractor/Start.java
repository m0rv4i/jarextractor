/**
 *  Copyright Murex S.A.S., 2003-2014. All Rights Reserved.
 * 
 *  This software program is proprietary and confidential to Murex S.A.S and its affiliates ("Murex") and, without limiting the generality of the foregoing reservation of rights, shall not be accessed, used, reproduced or distributed without the
 *  express prior written consent of Murex and subject to the applicable Murex licensing terms. Any modification or removal of this copyright notice is expressly prohibited.
 */
package com.rbone.jarextractor;

import java.io.File;
import java.io.IOException;

import java.util.Map;


/**
 * Tool used to recursively extract all none-test jars from a directory.
 */
public class Start {

    //~ ----------------------------------------------------------------------------------------------------------------
    //~ Static fields/initializers 
    //~ ----------------------------------------------------------------------------------------------------------------

    private static final String INPUT_DIRECTORY_KEY = "inputDirectory";
    private static final String OUTPUT_DIRECTORY_KEY = "outputDirectory";

    //~ ----------------------------------------------------------------------------------------------------------------
    //~ Methods 
    //~ ----------------------------------------------------------------------------------------------------------------

    /**
     * Entry point for the program.
     *
     * @param  args - the arguments passed to the program
     *
     * @throws IOException - when copying the jars errors.
     */
    public static void main(final String[] args) throws IOException {
        final Map<String, String> parsedArguments = loadArguments(args);
        final String startDirPath = (parsedArguments.get(INPUT_DIRECTORY_KEY) != null) ? parsedArguments.get(INPUT_DIRECTORY_KEY) : ".";
        final String outputDir = parsedArguments.get(OUTPUT_DIRECTORY_KEY);
        final File startDirectory = new File(startDirPath);
        if (!startDirectory.exists()) {
            throw new IllegalArgumentException("Start directory does not exist: " + startDirPath);
        }
        final JarExtractor jarExtractor = new JarExtractor(startDirPath, outputDir);
        jarExtractor.start();
        System.exit(0);
    }

    private static Map<String, String> loadArguments(final String[] args) {
        final Map<String, String> parsedArguments = ArgumentParser.parseArguments(args);
        if (!parsedArguments.containsKey(OUTPUT_DIRECTORY_KEY)) {
            printHelp();
            System.exit(0);
        }
        return parsedArguments;
    }

    private static void printHelp() {
        System.err.println("This tool is used to recursively copy all none-test jar files from a directory to an output directory");
        System.err.println(" **************************************************** ");
        System.err.println("Mandatory parameters: ");
        System.err.println("	-outputDirectory=<outputDirectoryPath>");
        System.err.println("Optional parameters: ");
        System.err.println("	-inputDirectory=<inputDirectoryPath>");
    }
}
