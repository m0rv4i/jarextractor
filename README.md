JarExtractor
==============

This tool is used to recursively extract none test jar files from a directory and copy them to an output directory.

Mandatory parameters:
* -inputDirectory=inputDirectoryPath

Optional parameters:
* -outputDirectory=outputDirectoryPath

If no output directory is specified the working directory will be used.

Test jars are determined by checking if the name ends with "-tests.jar", which is the normal maven format for test jars.

To build the tool:

* Ensure Apache Maven is installed and working.
* Run *mvn clean install* from the root directory
* Run java -jar on the generated executable file in target/jarExtractor-1.0-SNAPSHOT-jar-with-dependencies.jar

